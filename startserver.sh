#!/bin/bash
CURRENT_IP=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')
echo $CURRENT_IP
python manage.py runserver $CURRENT_IP:5085
#gunicorn mysite.wsgi:application -b 192.168.0.21:8085

