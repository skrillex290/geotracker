from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.shortcuts import render_to_response,redirect
from django.core.urlresolvers import reverse
from django.utils import feedgenerator

from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core import serializers
from datetime import datetime, timedelta
from django.contrib.auth.models import User
from django.core.mail import send_mail #send email
from django.core.mail import EmailMultiAlternatives
from django.contrib.admin.views.decorators import staff_member_required
from django.db.models import Count
from mainsite.settings import BASE_DIR
#app specific
from random import choice
import base64
import sys
import re
import json
from .models import * 
#############################################################################
# Create your views here.
DEFAULT_FROM_EMAIL  = "mailbox@a20.co.za"
CURRENT_PATH = sys.path[0] 
def get_client_ip(request):
     	if 'HTTP_X_FORWARDED_FOR' in request.META:
		ip = request.META['HTTP_X_FORWARDED_FOR'].split(",")[0].strip()
	else:
		ip = request.META.get('REMOTE_ADDR')
	return ip
def generate_random_username(length=16, chars="abcdefghjkmnpqrstuvwxyz23456789", split=4,delimiter=''):
    	username = ''.join([choice(chars) for i in xrange(length)])
    	if split:
        	username = delimiter.join([username[start:start+split] for start in range(0, len(username), split)])
    	try:
        	User.objects.get(username=username)
        	return generate_random_username(length=length, chars=chars, split=split, delimiter=delimiter)
    	except User.DoesNotExist:
        	return username;

def view_or_basicauth(view, request, test_func, realm = "", *args, **kwargs):
    """
    This is a helper function used by both 'logged_in_or_basicauth' and
    'has_perm_or_basicauth' that does the nitty of determining if they
    are already logged in or if they have provided proper http-authorization
    and returning the view if all goes well, otherwise responding with a 401.
    """
    if test_func(request.user):
        # Already logged in, just return the view.
        #
        return view(request, *args, **kwargs)

    # They are not logged in. See if they provided login credentials
    #
    if 'HTTP_AUTHORIZATION' in request.META:
        auth = request.META['HTTP_AUTHORIZATION'].split()
        if len(auth) == 2:
            # NOTE: We are only support basic authentication for now.
            #
            if auth[0].lower() == "basic":
                uname, passwd = base64.b64decode(auth[1]).split(':')
                user = authenticate(username=uname, password=passwd)
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        request.user = user
                        return view(request, *args, **kwargs)

    # Either they did not provide an authorization header or
    # something in the authorization attempt failed. Send a 401
    # back to them to ask them to authenticate.
    #
    response = HttpResponse()
    response.status_code = 401
    response['WWW-Authenticate'] = 'Basic realm="%s"' % realm
    return response

#############################################################################

def logged_in_or_basicauth(realm = ""):
    """
    A simple decorator that requires a user to be logged in. If they are not
    logged in the request is examined for a 'authorization' header.

    If the header is present it is tested for basic authentication and
    the user is logged in with the provided credentials.

    If the header is not present a http 401 is sent back to the
    requestor to provide credentials.

    The purpose of this is that in several django projects I have needed
    several specific views that need to support basic authentication, yet the
    web site as a whole used django's provided authentication.

    The uses for this are for urls that are access programmatically such as
    by rss feed readers, yet the view requires a user to be logged in. Many rss
    readers support supplying the authentication credentials via http basic
    auth (and they do NOT support a redirect to a form where they post a
    username/password.)

    Use is simple:

    @logged_in_or_basicauth
    def your_view:
        ...

    You can provide the name of the realm to ask for authentication within.
    """
    def view_decorator(func):
        def wrapper(request, *args, **kwargs):
            return view_or_basicauth(func, request,
                                     lambda u: u.is_authenticated(),
                                     realm, *args, **kwargs)
        return wrapper
    return view_decorator



############################################################################



@logged_in_or_basicauth()
def restricted(request):
	return HttpResponse("Since you're logged in, you can see this text!")

@logged_in_or_basicauth()
def registerposition(request):
	#longitude, latitude, accuracy, speed, altitude, direction, loc_timestamp, req_timestamp, offset, customfield1, customfield2
	if request.method == 'POST':
		o = PositionUpdate()
		if 'req_timestamp' in request.POST:
			updatetimestamp = request.POST['req_timestamp']
			o.UpdateTimeStamp = int(updatetimestamp)
		if 'longitude' in request.POST:
			longitude = request.POST['longitude']
			o.Longitude = longitude
		if 'latitude' in request.POST:
			latitude = request.POST['latitude']
			o.Latitude = latitude
		if 'accuracy' in request.POST:
			accuracy = request.POST['accuracy']
			o.Accuracy = accuracy
		if 'speed' in request.POST:
			speed = request.POST['speed']
			o.Speed = speed
		if 'altitude' in request.POST:
			altitude = request.POST['altitude']
			o.Altitude = altitude
		if 'bearing' in request.POST:
			bearing = request.POST['bearing']
			o.Bearing = bearing
		if 'loc_timestamp' in request.POST:
			loc_timestamp = int(request.POST['loc_timestamp'])
			o.PositionTimeStamp = loc_timestamp
		if 'deviceid' in request.POST:
			o.DeviceName = request.POST['deviceid']
		o.UpdateUser = 	request.user
		o.save()
		return HttpResponse(status=200,content="Ok")
	else:
		return HttpResponse(status=403,content="Must make a POST")

@staff_member_required
def approvals(request):	
	context = RequestContext(request)
	return render_to_response('approvals.html',context=context)

@login_required
def viewlocations(request):
	context = RequestContext(request)
	mostrecentdevice = PositionUpdate.objects.filter(UpdateUser = request.user).order_by('-UpdateTimeStamp','-PositionTimeStamp')
	positiondata = PositionUpdate.objects.filter(UpdateUser = request.user).order_by('-UpdateTimeStamp','-PositionTimeStamp')
	devices = PositionUpdate.objects.filter(UpdateUser=request.user).values('DeviceName').distinct()
	results = {'positiondata':positiondata, 'devices':devices}
	return render_to_response('viewlocations.html',results, context)

@logged_in_or_basicauth()
def getjson(request):
	import json
	context = RequestContext(request)
	#if request.META.HTTP_REFERER.find("a20.co.za") > -1:
	#	return HttpResponse(status=403)
	data ={}
	pageNumber = int(request.GET.get("page",1))
	startRecords = int((pageNumber-1) * int(ENDLESS_PAGINATION_PER_PAGE))
	endRecords  = int(startRecords + (pageNumber * int(ENDLESS_PAGINATION_PER_PAGE)))
	positiondata = PositionUpdate.objects.filter(UpdateUser=request.user).order_by('-UpdateTimeStamp', '-PositionTimeStamp')[startRecords:endRecords]
	#datetime.utcfromtimestamp(self.GivenLocationTimeStamp)
	for p in positiondata:
		l = datetime.utcfromtimestamp(p.PositionTimeStamp)
		t = datetime.utcfromtimestamp(p.UpdateTimeStamp)
		data[p.id] = {'longitude':p.Longitude, 'latitude':p.Latitude,'updateutc':t.strftime('%Y-%m-%d %H:%M:%S'), 'locationutc':l.strftime('%Y-%m-%d %H:%M:%S')}
	positiondata = json.dumps(data)
	return HttpResponse(positiondata, mimetype='application/json')

def getupdatedays(request):
	if not request.user.is_authenticated:
		return HttpResponse("Not authenticated.", 401) 
	#context = RequestContext(request)
	data = PositionUpdate.objects.filter(UpdateUser_id=request.user.id).extra({'Date':"date(ServerUpdateTime)"}).values('Date').annotate(Count=Count('id')).order_by('-Date')[:10]
	print data
	results = []
	for p in data:
		results.append({'Date':p['Date'], 'Count':p['Count']})
	results =json.dumps(results, sort_keys=True)
	return HttpResponse(results,content_type='application/json')
def getpositiondatabyday(request, dt, limit=20):
	if not request.user.is_authenticated:
		return HttpResponse("Not authenticated.", 401)
	s = dt.split('-')
	y,m,d = int(s[0]),int(s[1]),int(s[2])
	sd = datetime(y,m,d).date()	
	ed = sd + timedelta(days=1)	
	context = RequestContext(request)
	data = PositionUpdate.objects.filter(UpdateUser_id=request.user.id).filter(ServerUpdateTime__gt=sd).filter(ServerUpdateTime__lt=ed).order_by('-ServerUpdateTime')[:limit]
	results = []
	for d in data:
		results.append({'Latitude':d.Latitude, 'Longitude':d.Longitude})
	results = json.dumps(results, sort_keys=True)
	return HttpResponse(results, content_type='application/json')

def positiondatabydayasgeojson(request,dt,limit=30):
	if not request.user.is_authenticated:
		return HttpResponse("Not authenticated.",401)
	s = dt.split('-')
	y,m,d = int(s[0]),int(s[1]),int(s[2])
	sd = datetime(y,m,d).date()	
	ed = sd + timedelta(days=1)	
	context = RequestContext(request)
	data = PositionUpdate.objects.filter(UpdateUser_id=request.user.id).filter(ServerUpdateTime__gt=sd).filter(ServerUpdateTime__lt=ed).order_by('-ServerUpdateTime')[:limit]
	results =[]
	for d in data:
    		results.append("[%s,%s]"%(d.Latitude, d.Longitude))	
	tpl =	"""
					{
				"type": "FeatureCollection",
				"features": [{
					"geometry": {
						"type": "GeometryCollection",
						"geometries": [{
							"projection": "EPSG:3857",
							"type": "MultiLineString",
							"coordinates": ["""
	tpl += ",".join(results)						
							
	tpl += """]
						}]
					},
					"type": "Feature",
					"properties": {}
				}]
			}
		"""
	results = json.dumps(tpl, sort_keys=True)
	return HttpResponse(tpl,content_type='application/json')






def approve(request,id):
	if not request.user.is_staff:
		return HttpResponse("No privilege.",status=401)   			
	acc = AccountApproval.objects.get(pk=id)
	if acc:
		send_registration_information(acc.UserToBeApproved.username, acc.UserToBeApproved.username, acc.UserToBeApproved.email)
		acc.ApprovalStatus = True	
		acc.ApprovedDate = datetime.now()	
		acc.save()
	else:
		return HttpResponse("Invalid approval id", status=404)	
	return HttpResponse("Approved",status=200)

def getapprovalslist(request):
	if not request.user.is_staff:
		return HttpResponse("Not authenticated.",status=401)    			
	#context = RequestContext(request)
	results = []
	aa = AccountApproval.objects.filter(ApprovalStatus=False).order_by('-CreateDate')
	for a in aa:
		results.append({'IpAddress':a.IPAddress,'Email':a.UserToBeApproved.email,'Id':a.id, 'Username':a.UserToBeApproved.username, 'CreatedDate':a.CreateDate.strftime('%Y-%m-%d %T')})
	results = json.dumps(results, sort_keys=True)
	return HttpResponse(results,content_type='application/json')

@login_required
def logoutuser(request):
	logout(request)
	return HttpResponseRedirect(reverse('index'))

def loginuser(request):
	context = RequestContext(request)
	if request.method == 'POST':
		username = request.POST.get('txUsername')
		password = request.POST.get('txPassword')		
		user = authenticate(username=username,password=password)
		if user is not None:
			if user.is_active:				
				login(request, user)
				return HttpResponseRedirect(reverse('index'))
			else:
				print "valid user but account not active" 
				messages.warning(request, 'Incorrect username and/or password combination')
				return HttpResponse(reverse('login'))
		else:
				messages.warning(request, 'Incorrect username and/or password combination')
				return HttpResponse(reverse('login'))
	else:
		return render_to_response('login.html',{},context)

def getRegistrationFileContent(filename):
	file = open(filename,'r')
	return file.read()

def send_registration_information(username, password, emailaddress):
	subject,from_email,to = 'Your %s account details' % ('GEOTRACK'),'mailbox@a20.co.za',[emailaddress,DEFAULT_FROM_EMAIL]
	text_content =  getRegistrationFileContent(BASE_DIR + '/email.txt') % (username, password)
	html_content = getRegistrationFileContent(BASE_DIR+'/email.html') % (username, password)
	msg = EmailMultiAlternatives(subject, text_content, from_email, to)
	msg.attach_alternative(html_content, "text/html")
	msg.send()

def send_new_registration_notification(emailaddress, ip):
	subject,from_email,to = 'A new user has registered (%s - %s)' % (emailaddress, ip),'mailbox@a20.co.za',[DEFAULT_FROM_EMAIL]
	text_content =  getRegistrationFileContent(BASE_DIR + '/emailnewregistration.txt')
	html_content = getRegistrationFileContent(BASE_DIR+'/emailnewregistration.html')
	msg = EmailMultiAlternatives(subject, text_content, from_email, to)
	msg.attach_alternative(html_content, "text/html")
	msg.send()

def register(request):
    context = RequestContext(request)
    registered = False
    errors = []
    if request.method == 'POST':    			
        email = request.POST.get('txEmail')
        retypedemail = request.POST.get('txEmailConfirm')
        if email != retypedemail:
            errors.append("Email addresses do not match!")
        else:
			username = generate_random_username(8)
			password = "password" #default password for unapproved/inactive account
			print email
			user = User.objects.create_user(username=username,email=email, password=username) #user created
			user.is_active = False #requires approval
			user.save()
			a = AccountApproval()
			a.ApprovalStatus = False
			a.ApprovedDate = datetime(1999,9,9)
			a.UserToBeApproved = user
			a.IPAddress = get_client_ip(request)
			a.save()
			registered= True
			send_new_registration_notification(email, a.IPAddress)
			messages.success(request,'You will receive an email when your account is approved.')
			return HttpResponseRedirect(reverse('login'))
    return render_to_response('register.html',{'errors':errors, 'registered':registered},context)



def index(request):
	context = RequestContext(request)
	context_dict = {}
	return render_to_response('index.html',context_dict,context)


def about(request):
	context = RequestContext(request)
	return render_to_response('about.html',{},context)

def closemyaccount(request):
	context =  RequestContext(request)
	return render_to_response('closeaccount.html',{},context)