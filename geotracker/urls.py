from django.conf.urls import patterns, url
from geotracker import views

urlpatterns = patterns('',
	url(r'^$',views.index, name='index'),
	url(r'^register$',views.register, name='register'),
	#url(r'^list/',views.list,name='list'),
	url(r'^about', views.about, name='about'),
	#url(r'^add_category/$',views.add_category,name='add_category'),
    #url(r'^registerbyemail/$', views.registerbyemail,name='registerbyemail'),
	url(r'^login$',views.loginuser, name='login'),
	url(r'^logout$',views.logoutuser,name='logoutuser'),
	#url(r'^restricted/',views.restricted,name='restricted'),
	#url(r'^pu/',views.registerposition,name='registerposition'),
	url(r'^viewlocations',views.viewlocations,name='viewlocations'),
    url(r'^approvals',views.approvals,name='approvals'),
	url(r'^getapprovalslist',views.getapprovalslist,name='getapprovalslist'),
	url(r'^getupdatedays',views.getupdatedays,name='getupdatedays'),
	url(r'^approve/(?P<id>\d+)/$',views.approve,name='approve'),
	url(r'^getpositiondatabyday/(?P<dt>[0-9]{4}-[0-9]{2}-[0-9]{2})/$',views.getpositiondatabyday,name='getpositiondatabyday'),
	url(r'^getpositiondatabyday/(?P<dt>[0-9]{4}-[0-9]{2}-[0-9]{2})/(?P<limit>\d+)/$',views.getpositiondatabyday,name='getpositiondatabyday'),
	url(r'^closeaccount$',views.closemyaccount,name='closemyaccount'),	
	url(r'^getpositiondatabydayasgeojson/(?P<dt>[0-9]{4}-[0-9]{2}-[0-9]{2})/(?P<limit>\d+)/app.json$',views.positiondatabydayasgeojson,name='getpositiondatabydayasgeojson'),
	#url(r'^getjson/',views.getjson,name='getjson'),	
)

