from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, timedelta

# Create your models here.

class AccountApproval(models.Model):
    UserToBeApproved = models.ForeignKey(User)
    CreateDate = models.DateTimeField(auto_now_add=True)
    ApprovalStatus = models.BooleanField(default=False)
    ApprovedDate = models.DateTimeField(blank=True)
    IPAddress= models.CharField(max_length=40)
    def __unicode__(self):
        return self.UserToBeApproved.username
    
class PositionUpdate(models.Model):
    UpdateUser = models.ForeignKey(User)
    ServerUpdateTime = models.DateTimeField(auto_now_add=True)
    PositionTimeStamp = models.IntegerField()
    UpdateTimeStamp = models.IntegerField()
    Longitude = models.CharField(max_length=15)
    Latitude = models.CharField(max_length=15)
    Bearing = models.CharField(max_length=20, null=True)
    Altitude = models.CharField(max_length=20, null=True)
    Speed  = models.CharField(max_length=20, null=True)
    Accuracy = models.CharField(max_length=20, null=True)
    DeviceName = models.CharField(max_length=10, null=True)
    def PositionTimeStampAsDate(self):
        return datetime.utcfromtimestamp(self.PositionTimeStamp)
    def UpdateTimeStampAsDate(self):
        return datetime.utcfromtimestamp(self.UpdateTimeStamp)
    def __unicode__(self):
        return "Longitude: %s Latitude: %s"%(self.Longitude, self.Latitude)

