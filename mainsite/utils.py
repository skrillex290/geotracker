from random import choice
from string import ascii_lowercase, digits
from django.contrib.auth.models import User

def get_client_ip(request):
 	if 'HTTP_X_FORWARDED_FOR' in request.META:
		ip = request.META['HTTP_X_FORWARDED_FOR'].split(",")[0].strip()
	else:
		ip = request.META.get('REMOTE_ADDR')
	print "setting ip as %s" % ip
	return ip

def generate_random_username(length=16, chars="abcdefghjkmnpqrstuvwxyz23456789", split=4,delimiter=''):
	username = ''.join([choice(chars) for i in xrange(length)])
    	if split:
        	username = delimiter.join([username[start:start+split] for start in range(0, len(username), split)])
    	try:
        	User.objects.get(username=username)
        	return generate_random_username(length=length, chars=chars, split=split, delimiter=delimiter)
    	except User.DoesNotExist:
        	return username

def generate_random_password(chars="ABCDEFGHJKLMNPQRSTUVWXYZ23456789!@#$%+()", padlength=5):
    words = (
    "signal", "shelter", "shade", "melted",
    "cure",    "grip",     "naughty","clear",
    "ruin",    "childlike",    "actually","skillful",
    "cagey","xkcd",    "proud",    "heal","perpetual",
    "list",    "face",    "modern","flood",
    "opposite", "fact",    "abiding","adorable",
    "glove",    "like",    "connect",    "grade",
    "knit",    "improve",    "seemly",    "crash",
    "relieved",    "hydrant",    "income",    "dapper",
    "cultured",    "knife",    "dazzling",    "obedient",
    "decorous",    "right",    "organic" ,"griffon","laugh",
    "crib",    "match",    "bury",    "marble",
    "talented", "blood",   "approve",    "cruel",    "kneel",
    "zipper",    "sturdy",    "number",    "wary","wakeford",
    "deafening",    "even",    "vein",    "business",
    "minute",    "fluttering",    "devilish",    "zebra",
    "meat",   "fungus", "enjoy",    "drip",    "rice",
    "small",    "waiting",    "judge",    "rose",
    "useful",    "daughter",    "fantastic",    "dramatic",
    "deliver",    "valuable",    "elastic",    "rob",
    "upset",   "truffle", "event",    "move",    "spoon","beach",
    "homeless",  "lemon",  "glamorous",    "legal",    "loose","toenail",
    "light",    "squirrel",    "maddening",    "interrupt",
    "playground",  "spanner",  "suit",    "turkey",    "smell", "money",
    "summer",    "sort",    "uncle",  ,"indelible","manly","gecko", "society")
    return ''.join([choice(words) for i in xrange(2)]) + ''.join([choice(chars) for i in xrange(3)])
